import React, { useState } from 'react';
import axios from 'axios';
// import form from './../RegisterForm/form.css';
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import jwt_decode from 'jwt-decode';
import { useDispatch } from 'react-redux';
import { setUser } from '../../redux/user/userActions';

const Login = (props) => {
  const [state, setState] = useState({
    email: '',
    password: '',
    errors: '',
  });
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    const formData = {
      email: state.email,
      password: state.password,
    };

    axios
      .post('http://localhost:8000/api/v1/login', formData)
      .then((response) => {
        if (response.data.errors) {
          setState({
            ...state,
            errors: response.data.errors,
            password: '',
          });
        } else {
          // console.log(response, 'res');

          var decoded = jwt_decode(response.data.token);
          console.log('decoded', decoded);
          dispatch(setUser(decoded));
          // write this to localStorage
          localStorage.setItem('token', response.data.token);
          let tokenData = response.data.token;
          console.log(tokenData);
          // redirect to notes page

          // change the navigation links = update the state of isAuthenticated in the parent component
          props.handleAuthentication(true);
          props.history.push('/api/v1/login_token');
        }
      });
  };

  const handleChange = (e) => {
    e.persist();
    setState({
      ...state,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div>
      <div className='formheader col-md-6'>
        <h2>Login </h2>
        <Form onSubmit={handleSubmit}>
          {state.errors && <p className='formcenter'>{state.errors}</p>}
          <div>
            <FormGroup row>
              <Label sm={2} className='headerlabel'>
                Email :
              </Label>

              <Col sm={10}>
                <Input
                  type='text'
                  name='email'
                  value={state.email}
                  onChange={handleChange}
                  className='form-control'
                  placeholder='Enter email'
                ></Input>
              </Col>
            </FormGroup>
          </div>
          <div>
            <FormGroup row>
              <Label sm={2} className='headerlabel'>
                Password:
              </Label>
              <Col sm={10}>
                <Input
                  type='password'
                  name='password'
                  value={state.password}
                  onChange={handleChange}
                  className='form-control'
                  placeholder='your password'
                ></Input>
              </Col>
            </FormGroup>
          </div>
          <Button color='primary'>Submit</Button>
        </Form>
      </div>
    </div>
  );
};
export default Login;
