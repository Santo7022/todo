import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

const ToDo = () => {
  const [list, setlist] = useState([]);
  const dispatch = useDispatch();
  const userRole = useSelector((state) => state.user.role);
  useEffect(() => {
    axios
      .get('http://localhost:8000/api/v1/getall', {
        headers: {
          'Auth-Controller': localStorage.getItem('token'),
        },
      })
      .then((response) => {
        console.log(response.data);
        const list = response.data.data;
        setlist(list);
      });
  }, []);

  return (
    <div>
      <h2>Listing ToDo- {list.length}</h2>
      {list.length === 0 ? (
        <div>No ToDo found</div>
      ) : (
        <ul>
          {list.map((note) => {
            console.log(note, 'in notes list');
            return (
              <li key={note._id}>
                {<h2>Title :{note.title}</h2>}
                {<h4>Body :{note.body}</h4>}
                {<h4>Id{note._id}</h4>}

                {/* <Link to ={`/notes/${note._id}`}>
                               
                            </Link> */}
                {userRole === 'admin' && (
                  <Link to={`/api/v1/delete?id=${note._id}`}>Remove</Link>
                )}
                {userRole === 'admin' && (
                  <Link to={`/notes/edit/${note._id}`}>Edit</Link>
                )}
              </li>
            );
          })}
        </ul>
      )}
      <Link to='/api/v1/create'>Add ToDO</Link>
    </div>
  );
};

export default ToDo;
