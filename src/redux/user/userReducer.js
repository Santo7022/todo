import { SET_USER } from './userTypes';

const initialState = {
  role: 'default',
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER: {
      console.log('ss', state.role);
      return {
        ...state,
        role: action.payload.role,
      };
    }

    default:
      return state;
  }
};

export default userReducer;
